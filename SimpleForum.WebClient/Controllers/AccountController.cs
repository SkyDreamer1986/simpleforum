﻿using System;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Extensions;
using Microsoft.Owin;
using System.Web;
using System.Linq;

namespace SimpleForum.WebClient.Controllers
{
    using Models;
    using System.Net;
    using Infrastructure.Security;
    using Domain;
    using DataAccessLayer;

    [Authorize]
    public class AccountController : Controller
    {
        UserManager<IdentityUser, Guid> applicationUserManager;
        IUnitOfWork unitOfWork;

        public AccountController(UserManager<IdentityUser, Guid> applicationUserManager, IUnitOfWork unitOfWork)
        {
            this.applicationUserManager = applicationUserManager;
            this.unitOfWork = unitOfWork;
        }

        public IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register(string redirectUrl)
        {
            ViewBag.RedirectUrl = redirectUrl;
            var rvm = new RegisterViewModel { CountryList = GetCountryList() };
            return View(rvm);
        }

        [NonAction]
        private SelectList GetCountryList(Guid? personCountryOfLiving = null)
        {
            var result = new SelectList(unitOfWork.Countries.GetAll(), "Id", "ShortName");

            if (personCountryOfLiving != null)
                result.Where(c => Guid.Parse(c.Value) == personCountryOfLiving).FirstOrDefault().Selected = true;

            return result;
        }

        [NonAction]
        private SelectList GetGenderList(Guid? personGender = null)
        {
            var result = new SelectList(unitOfWork.Genders.GetAll(), "Id", "Name");
            
            if (personGender != null)
                result.Where(g => Guid.Parse(g.Value) == personGender).First().Selected = true;

            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel registrationInfo, string redirectUrl)
        {
            if (ModelState.IsValid)
            {
                // TODO: Нехороший подход, нужно разобраться, где и как создавать сущности. Читай "Внедрение зависимостей в .NET"
                var newPerson = new Person
                {
                    BirthDate = registrationInfo.BirthDate,
                    CountryOfLivingId = registrationInfo.CountryOfLivingId,
                    GenderId = registrationInfo.GenderId,
                    FirstName = registrationInfo.FirstName,
                    SecondName = registrationInfo.SecondName,
                    LastName = registrationInfo.LastName
                };

                // Регистрируем пользователя
                IdentityUser identityUser = new IdentityUser()
                {
                    UserName = registrationInfo.UserName,
                    Email = registrationInfo.Email,
                    EmailConfirmed = false,
                    Blocked = false,
                    Person = newPerson
                };
                
                var result = await applicationUserManager.CreateAsync(identityUser, registrationInfo.Password);
                if (result.Succeeded)
                {
                    var identity = await applicationUserManager.CreateIdentityAsync(identityUser, DefaultAuthenticationTypes.ApplicationCookie);

                    var registeredUser = await applicationUserManager.FindByNameAsync(registrationInfo.UserName);

                    var result2 = await applicationUserManager.AddToRoleAsync(registeredUser.Id, "User");

                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);
                    if (string.IsNullOrEmpty(ViewBag.RedirectUrl) )
                        return await Task.FromResult(RedirectToAction("SuccessfullRegistration"));
                    else
                        return await Task.FromResult(Redirect(ViewBag.RedirectUrl));
                }

                return View();
            }
            else             
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult EditUserInfo()
        {
            if (Request.IsAuthenticated)
            {
                var userName = Request.RequestContext.HttpContext.User.Identity.GetUserName();
                var identityUser = applicationUserManager.FindByName(userName);

                var regInfo = new EditUserInfoViewModel()
                {
                    FirstName = identityUser.Person.FirstName,
                    SecondName = identityUser.Person.SecondName,
                    LastName = identityUser.Person.LastName,
                    BirthDate = identityUser.Person.BirthDate,
                    GenderList = GetGenderList(identityUser.Person.GenderId),
                    GenderId = identityUser.Person.GenderId,
                    CountryList = GetCountryList(identityUser.Person.CountryOfLivingId),
                    CountryOfLivingId = identityUser.Person.CountryOfLivingId,
                    Email = identityUser.Email
                };

                return View(regInfo);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult EditUserInfo(EditUserInfoViewModel userInfo)
        {
            if (ModelState.IsValid)
            {
                userInfo.CountryList = GetCountryList(userInfo.CountryOfLivingId);
                userInfo.GenderList = GetGenderList(userInfo.GenderId);

                Guid userId = Guid.Parse(Request.RequestContext.HttpContext.User.Identity.GetUserId());

                var identityUser = applicationUserManager.FindById(userId);

                if (identityUser != null)
                {
                    identityUser.Person.FirstName = userInfo.FirstName;
                    identityUser.Person.SecondName = userInfo.SecondName;
                    identityUser.Person.LastName = userInfo.LastName;
                    identityUser.Person.BirthDate = userInfo.BirthDate;
                    identityUser.Person.GenderId = userInfo.GenderId;

                    identityUser.Person.CountryOfLivingId = userInfo.CountryOfLivingId;
                    identityUser.EmailConfirmed = userInfo.Email != identityUser.Email ? false : identityUser.EmailConfirmed;
                    identityUser.Email = userInfo.Email;
                }

                applicationUserManager.Update(identityUser);
            }

            return View(userInfo);
        }

        [AllowAnonymous]
        public ActionResult Login(string redirectUrl)
        {
            ViewBag.RedirectUrl = redirectUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel loginInfo, string redirectUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await applicationUserManager.FindAsync(loginInfo.UserName, loginInfo.Password);                
                if (user != null)
                {
                    var identity = await applicationUserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = false }, identity);
                    if (string.IsNullOrEmpty(redirectUrl))
                        return await Task.FromResult(RedirectToAction("Index", "Home"));
                    else
                        return await Task.FromResult(Redirect(redirectUrl));
                }
            }

            return View();
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
        

        [AllowAnonymous]
        public ActionResult SuccessfullRegistration()
        {
            return View();
        }

        #region





        #endregion
    }
}