﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace SimpleForum.WebClient.Controllers
{
    using Domain;
    using DataAccessLayer;
    using Models;

    public class TopicController : Controller
    {
        IUnitOfWork unitOfWork;
        public TopicController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        
        private Guid? CurrentUserId
        {
            get
            {
                var userId = Request.RequestContext.HttpContext.User.Identity.GetUserId();
                if (!string.IsNullOrEmpty(userId)) return Guid.Parse(userId); else return null;
            }
        }

        private bool IsUserAuthenticated { get { return Request.IsAuthenticated; } }


        public ActionResult Index(Guid? topicId, string errorText = "")
        {
            if (topicId == null)
                return RedirectToAction("Index", "Home");

            var topic = unitOfWork.Topics.GetTopicWithPosts(topicId?? Guid.NewGuid());
            return View(new TopicPageViewModel { Topic = topic, ErrorText = errorText });
        }

        [HttpPost]
        public ActionResult Index(TopicPageViewModel topicPageVM)
        {
            return View(topicPageVM);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddPost(string message, Guid? topicId)
        {
            return DoAction(topicId, null, OperationType.Add, message);
        }

        enum OperationType
        {
            Add = 0, Update, Delete
        }
        
        [NonAction]
        private ActionResult DoAction(Guid? topicId, Guid? postId, OperationType opType, string content = "")
        {
            bool hasError = false;
            string errorText = "";

            // Проверка возможности совершения операции
            // TODO: Это наши бизнес-правила, если можно так выразиться. Может, они должны проверяться методами моделей?
            // Можем добавить непустое сообщение или заменить сообщение на непустое
            if (string.IsNullOrEmpty(content) && (opType == OperationType.Add || opType == OperationType.Update))
            {
                hasError = true;
                errorText = "Сообщение не может быть пустым!";

                return RedirectToAction("Index", "Topic", new { topicId = topicId, errorText = errorText });
            }

            // Выполнение операции
            if (!hasError)
            {
                Post post = new Post();

                if (opType == OperationType.Update || opType == OperationType.Delete)
                {
                    post = unitOfWork.Posts.Find(p => p.Id == postId).FirstOrDefault();
                    topicId = post.TopicId; // Нам нужно будет перейти в тему изменяемого / удяляемого поста
                }


                switch (opType)
                {
                    case OperationType.Add:
                        unitOfWork.Posts.Add(new Post
                        {
                            Id = Guid.NewGuid(),
                            CreationDate = DateTime.Now,
                            TopicId = topicId ?? Guid.NewGuid(), // Не очень хороший вариант, нужно подумать, как его заменить
                            UserId = CurrentUserId ?? Guid.NewGuid(), // И снова не очень хороший вариант.
                            Content = content
                        });
                        break;
                    case OperationType.Update:
                        
                        post.Content = content;
                        break;
                    case OperationType.Delete:
                        unitOfWork.Posts.Remove(post);
                        break;
                    default:
                        break;
                }

                unitOfWork.Complete();
            }

            return RedirectToAction("Index", "Topic", new { topicId = topicId });
        }

        [Authorize]
        public ActionResult UpdatingPost(TopicPageViewModel editingTopicPage, Guid? updatingPostId)
        {
            editingTopicPage.Topic.Posts = unitOfWork.Posts.Find(p => p.TopicId == editingTopicPage.Topic.Id).ToList();
            return View("Index", editingTopicPage);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdatePost(Guid postId, string updatedContent)
        {
            return DoAction(null, postId, OperationType.Update, updatedContent);
        }

        [Authorize]
        public ActionResult DeletePost(Guid postId)
        {
            return DoAction(null, postId, OperationType.Delete);
        }
    }
}