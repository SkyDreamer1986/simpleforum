﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleForum.WebClient.Controllers
{
    using Domain;
    using DataAccessLayer;
    using Models;

    public class HomeController : Controller
    {
        IUnitOfWork unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public ActionResult Index()
        {
            Forum forum = unitOfWork.Forums.GetCurrentForum(true, true);
            ViewBag.Title = forum.Title;
            return View(forum);
        }

        [Authorize]
        public ActionResult AddNewTopic(Guid forumSectionId)
        {
            TopicEditorViewModel newTopic = new TopicEditorViewModel
            {
                ForumSectionId = forumSectionId
            };

            return View();
        }
        
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult AddNewTopic(TopicEditorViewModel newTopic)
        {
            if (newTopic == null)
                HttpNotFound("Не указана тема для создания!");

            if (!HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Login");

            var user = unitOfWork.Users.Find(x => x.UserName == HttpContext.User.Identity.Name).FirstOrDefault();

            // :TODO В будущем создание объектов будет реализовано посредством специальной фабрики, чтобы код был в одном месте
            var topic = new Topic
            {
                Id = Guid.NewGuid(),
                CreationDate = DateTime.Now,
                ForumSectionId = newTopic.ForumSectionId,
                Title = newTopic.Title,
                Description = newTopic.Description,
                UserId = user.Id,
                User = user
            };

            unitOfWork.Topics.Add(topic);
            unitOfWork.Complete();

            // TODO: Переход нужен в новую тему, чтобы было удобнее в ней же уже что-нибудь написать

            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}