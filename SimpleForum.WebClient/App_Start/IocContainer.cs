﻿using System;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Microsoft.AspNet.Identity;

namespace SimpleForum.WebClient.App_Start
{
    using DataAccessLayer;
    using DataAccessLayer.Implementations;
    using Infrastructure.DI;
    using Infrastructure.Security;

    public static class IocContainer
    {
        public static void Configure()
        {
            IUnityContainer unityContainer = new UnityContainer();

            unityContainer.RegisterType<ApplicationContext>(new InjectionConstructor());

            unityContainer.RegisterType<IUnitOfWork, UnitOfWork>(new InjectionConstructor(unityContainer.Resolve(typeof(ApplicationContext))));

            unityContainer.RegisterType<ApplicationUserStore>(new InjectionConstructor(unityContainer.Resolve<IUnitOfWork>()));

            unityContainer.RegisterType<UserManager<IdentityUser, Guid>>(new InjectionConstructor(unityContainer.Resolve<ApplicationUserStore>()));

            DependencyResolver.SetResolver(new ApplicationDependencyResolver(unityContainer));
        }
    }
}