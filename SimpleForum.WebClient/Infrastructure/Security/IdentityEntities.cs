﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace SimpleForum.WebClient.Infrastructure.Security
{
    using Domain;
    public class IdentityUser : IUser<Guid>
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public Person Person { get; set; }

        public string Name { get { return Person != null ? Person.LastName + " " + Person.SecondName + " " + Person.FirstName : ""; } }

        public bool Blocked { get; set; }

        public string Email { get; set; }

        public bool? EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public IdentityUser()
        {
            Id = Guid.NewGuid();
            Person = new Person();
        }

        public IdentityUser(string userName, string passwordHash)
            :base()
        {
            UserName = userName;
            PasswordHash = passwordHash;
            Blocked = false;                
        }
    }


    public class IdentityRole : IRole<Guid>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }















}