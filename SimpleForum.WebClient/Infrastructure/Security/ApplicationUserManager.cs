﻿using System;
using System.Linq;
using Microsoft.AspNet.Identity;

namespace SimpleForum.WebClient.Infrastructure.Security
{
    using System.Threading.Tasks;
    using Domain;
    using DataAccessLayer;
    using System.Collections.Generic;

    public class ApplicationUserManager : UserManager<IdentityUser, Guid>
    {
        public ApplicationUserManager(ApplicationUserStore store)
            :base(store)
        {
        }
    }

    public class ApplicationUserStore: IUserStore<IdentityUser, Guid>, IUserPasswordStore<IdentityUser, Guid>, IUserRoleStore<IdentityUser, Guid>, IUserSecurityStampStore<IdentityUser, Guid>, IUserClaimStore<IdentityUser, Guid>
    {
        IUnitOfWork unitOfWork;

        public ApplicationUserStore(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        void fillIdentityUserFields(IdentityUser identityUser, User user)
        {
            identityUser.Id = user.Id;
            identityUser.UserName = user.UserName;
            identityUser.PasswordHash = user.PasswordHash;
            identityUser.SecurityStamp = user.SecurityStamp;
            identityUser.Email = user.Email;
            identityUser.EmailConfirmed = user.EmailConfirmed;
            identityUser.Blocked = user.Blocked;
            identityUser.Person = user.Person;
        }

        void fillUserFields(User user, IdentityUser identityUser)
        {
            user.Id = identityUser.Id;
            user.UserName = identityUser.UserName;
            user.Email = identityUser.Email;
            user.EmailConfirmed = identityUser.EmailConfirmed;
            user.PasswordHash = identityUser.PasswordHash;
            user.SecurityStamp = identityUser.SecurityStamp;
            user.Blocked = identityUser.Blocked;
            user.Person = identityUser.Person;
        }

        User getUser(IdentityUser identityUser)
        {
            if (identityUser == null)
                return null;

            return new User()
            {
                Id = identityUser.Id,
                UserName = identityUser.UserName,
                Email = identityUser.Email,
                PasswordHash = identityUser.PasswordHash,
                SecurityStamp = identityUser.SecurityStamp,
                Blocked = false,
                Person = identityUser.Person
            };
        }

        IdentityUser getIdentityUser(User user)
        {
            if (user == null)
                return null;

            return new IdentityUser
            {
                Id = user.Id,
                Blocked = user.Blocked,
                UserName = user.UserName,
                Email = user.Email,
                PasswordHash = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                Person =user.Person
            };
        }

        public Task CreateAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            else
            {
                unitOfWork.Users.Add(getUser(user));                
                return unitOfWork.CompleteAsync();
            }
        }

        public Task UpdateAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            
            
                var modifiedUser = unitOfWork.Users.Find(u => u.Id == user.Id).FirstOrDefault();

            if (modifiedUser == null)
                throw new ArgumentException(string.Format("Пользователь с указанным ID = {0} не найден в БД!", user.Id));
            
            fillUserFields(modifiedUser, user);
            return unitOfWork.CompleteAsync();
        }

        public Task DeleteAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var deletingUser = unitOfWork.Users.Get(user.Id);

            if (deletingUser == null)
                throw new ArgumentException(string.Format("Пользователь с указанным ID = {0} не найден в БД!", user.Id));

            unitOfWork.Users.Remove(deletingUser);

            return unitOfWork.CompleteAsync();
        }

        public Task<IdentityUser> FindByIdAsync(Guid userId)
        {
            var user = unitOfWork.Users.GetUserWithPerson(userId);
            return Task.FromResult(getIdentityUser(user));
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
            var user = unitOfWork.Users.GetUserWithPerson(userName);
            return Task.FromResult(getIdentityUser(user));
        }

        public Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            return Task.FromResult<string>(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }
        
        public Task SetSecurityStampAsync(IdentityUser user, string stamp)
        {
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }

        public Task<string> GetSecurityStampAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            return Task.FromResult(user.SecurityStamp);
        }

        public Task<bool> IsInRoleAsync(IdentityUser user, string roleName)
        {
            throw new NotImplementedException();
        }
        
        public Task AddToRoleAsync(IdentityUser user, string roleName)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var dbUser = unitOfWork.Users.Find(u => u.Id == user.Id).FirstOrDefault();
            if (dbUser == null)
                throw new ArgumentException(String.Format("Пользователь {0} не найден в БД!", user.UserName));

            var userIsAlreadyInRole = unitOfWork.Roles.GetUserRoles(user.Id).Where(r => r.Name.Equals(roleName)).FirstOrDefault() != null;
            if (!userIsAlreadyInRole)
            {
                dbUser.Roles.Add(unitOfWork.Roles.Find(r => r.Name == roleName).First());
                return unitOfWork.CompleteAsync();                
            }
            return Task.FromResult(0);
        }

        public Task RemoveFromRoleAsync(IdentityUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult((IList<string>)unitOfWork.Roles.Find(x => x.Users.Where(u => u.Id == user.Id).Count() > 0).Select(x => x.Name).ToList());
        }
             
        
        public Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var userClaims = unitOfWork.Claims.Find(x => x.UserId == user.Id);

            var identityClaims = userClaims.Select(x => new System.Security.Claims.Claim(x.ClaimType, x.ClaimValue)).ToList();
            
            return Task.FromResult((IList<System.Security.Claims.Claim>)identityClaims);

            throw new NotImplementedException();
        }

        public Task AddClaimAsync(IdentityUser user, System.Security.Claims.Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimAsync(IdentityUser user, System.Security.Claims.Claim claim)
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            unitOfWork.Dispose();
        }
    }
}