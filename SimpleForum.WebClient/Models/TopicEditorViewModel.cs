﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SimpleForum.WebClient.Models
{
    public class TopicEditorViewModel
    {
        public Guid ForumSectionId { get; set; }

        [Display(Name = "Название")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }
    }
}