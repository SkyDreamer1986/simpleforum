﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SimpleForum.WebClient.Models
{
    using Domain;
    
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        public string SecondName { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        public SelectList GenderList { get; set; }

        [Display(Name = "Пол")]
        public Guid? GenderId { get; set; }


        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }

        public SelectList CountryList { get; set; }

        [Display(Name = "Страна")]
        public Guid? CountryOfLivingId { get; set; }
        
        [Required]
        [Display(Name = "Логин")]
        public String UserName { get; set; }

        [Required]
        [Display(Name = "Почтовый адрес")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [Required]
        [Display(Name = "Подтверждение")]
        [DataType(DataType.Password)]
        public String ConfirmPassword { get; set; }

        public RegisterViewModel()
        {
            
        }
    }
    
    public class EditUserInfoViewModel
    {
        [Required]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Отчество")]
        public string SecondName { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }


        public SelectList GenderList { get; set; }

        [Display(Name = "Пол")]
        public Guid? GenderId { get; set; }


        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }

        public SelectList CountryList { get; set; }

        [Display(Name = "Страна")]
        public Guid? CountryOfLivingId { get; set; }

        [Required]
        [Display(Name = "Адрес электронной почты")]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        public string ResultOperationMessage { get; set; }

        public EditUserInfoViewModel()
        {

        }
    }

    public class LoginViewModel
    {
        [Display(Name = "Логин")]
        [Required]
        public String UserName { get; set; }

        [Display(Name = "Пароль", Prompt = "Пароль")]
        [DataType(DataType.Password)]
        [Required]
        public String Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }
    }

    public class RecoverPasswordViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Адрес электронной почты")]
        public string EmailAddress { get; set; }
    }



}