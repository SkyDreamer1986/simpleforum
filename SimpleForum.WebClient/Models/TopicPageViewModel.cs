﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleForum.WebClient.Models
{
    using Domain;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Модель страницы темы форума
    /// </summary>
    public class TopicPageViewModel
    {
        [Display(Name = "Тема")]
        public Topic Topic { get; set; }

        [Display(Name = "Текущая страница")]
        public int CurrentPage { get; set; }

        [Display(Name = "Количество постов на странице")]
        public int PostsPerPage { get; set; }

        [Display(Name = "Сообщение")]
        public String Message { get; set; }

        public Guid? UpdatingPostId { get; set; }

        [Display(Name = "Ошибка")]
        public string ErrorText { get; set; }

    }
}