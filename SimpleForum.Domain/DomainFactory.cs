﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.Domain
{
    public class DomainFactory : IDomainFactory
    {
        public Post CreatePost(Guid topicId, Guid userId, string message)
        {
            return new Post
            {
                Id = Guid.NewGuid(),
                CreationDate = DateTime.Now,
                TopicId = topicId,
                UserId = userId,
                Content = message 
            };
        }

        public Topic CreateTopic(Guid forumSectionId, Guid userId, string title, string description)
        {
            return new Topic
            {
                Id = Guid.NewGuid(),
                CreationDate = DateTime.Now,
                ForumSectionId = forumSectionId,
                Title = title,
                Description = description,
                UserId = userId
            };
        }
    }
}
