﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleForum.Domain
{
    public class Gender
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }

    public class Person
    {
        [ForeignKey("User")]
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }
        
        public Guid? CountryOfLivingId { get; set; }
        public Country CountryOfLiving { get; set; }

        public Guid? GenderId { get; set; }

        public virtual Gender Gender { get; set; }

        public virtual User User { get; set; }
    }

    public class Country
    {
        public Guid Id { get; set; }

        public string IsoCode { get; set; }

        public string ShortName { get; set; }

        public string FullName { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }
    
    public class User
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }
        public bool Blocked { get; set; }

        public string Email { get; set; }

        public bool? EmailConfirmed { get; set; }

        public virtual Person Person { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        public virtual ICollection<Claim> Claims { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

        public User()
        {
            Roles = new HashSet<Role>();
            Claims = new HashSet<Claim>();
            Posts = new HashSet<Post>();
            Topics = new HashSet<Topic>();
        }

        public User(string userName, string passwordHash, string securityStamp)
        {
            Id = Guid.NewGuid();
            UserName = userName;
            PasswordHash = passwordHash;
            SecurityStamp = securityStamp;
        }
    }

    public class Role
    {
        public Guid Id { get; set; }

        public String Name { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public Role()
        {
            Users = new HashSet<User>();
        }

    }

    public class Claim
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public virtual User User { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }
    }

    public class Post
    {
        public Guid Id  { get; set; }

        public String Content { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid TopicId { get; set; }
        public Topic Topic { get; set; }

        public DateTime CreationDate { get; set; }
    }

    public class Topic
    {
        public Topic()
        {
            Posts = new HashSet<Post>();
        }
        public Guid Id { get; set; }

        public String Title { get; set; }

        public Guid ForumSectionId { get; set; }
        public ForumSection ForumSection { get; set; }

        public DateTime CreationDate { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public string Description { get; set; }
    }

    public class ForumSection
    {
        public ForumSection()
        {
            Topics = new HashSet<Topic>();
        }
        public Guid Id { get; set; }

        public String Title { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

        public Guid ForumId { get; set; }
        public virtual Forum Forum { get; set; }

        public int? ShowOrder { get; set; }
    }

    public class Forum
    {
        public Forum()
        {
            ForumSections = new HashSet<ForumSection>();
        }

        public Guid Id { get; set; }

        public String Title { get; set; }

        public virtual ICollection<ForumSection> ForumSections { get; set; }

    }
}