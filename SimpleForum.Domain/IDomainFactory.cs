﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.Domain
{
    interface IDomainFactory
    {
        Post CreatePost(Guid topicId, Guid userId, string message);
        Topic CreateTopic(Guid forumSectionId, Guid userId, string title, string description);
    }
}
