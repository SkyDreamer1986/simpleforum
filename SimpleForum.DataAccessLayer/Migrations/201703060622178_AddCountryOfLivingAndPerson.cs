namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCountryOfLivingAndPerson : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        LastName = c.String(),
                        BirthDate = c.DateTime(),
                        CountryOfLivingId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryOfLivingId)
                .ForeignKey("dbo.Users", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.CountryOfLivingId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IsoCode = c.String(),
                        ShortName = c.String(),
                        FullName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "Id", "dbo.Users");
            DropForeignKey("dbo.People", "CountryOfLivingId", "dbo.Countries");
            DropIndex("dbo.People", new[] { "CountryOfLivingId" });
            DropIndex("dbo.People", new[] { "Id" });
            DropTable("dbo.Countries");
            DropTable("dbo.People");
        }
    }
}
