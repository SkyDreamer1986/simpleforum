namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPostsToUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "Claim_Id", "dbo.Claims");
            DropIndex("dbo.Posts", new[] { "Claim_Id" });
            DropColumn("dbo.Posts", "Claim_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Claim_Id", c => c.Guid());
            CreateIndex("dbo.Posts", "Claim_Id");
            AddForeignKey("dbo.Posts", "Claim_Id", "dbo.Claims", "Id");
        }
    }
}
