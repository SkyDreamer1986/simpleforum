namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShorOrderAndDisableNullForTopicCreationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topics", "ShowOrder", c => c.Int(nullable: false));
            AlterColumn("dbo.Topics", "CreationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Topics", "CreationDate", c => c.DateTime());
            DropColumn("dbo.Topics", "ShowOrder");
        }
    }
}
