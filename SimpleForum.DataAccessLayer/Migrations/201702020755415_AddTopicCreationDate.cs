namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTopicCreationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topics", "CreationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Topics", "CreationDate");
        }
    }
}
