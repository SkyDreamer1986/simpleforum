namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTopicDescription : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Topics", "User_Id", "dbo.Users");
            DropIndex("dbo.Topics", new[] { "User_Id" });
            RenameColumn(table: "dbo.Topics", name: "User_Id", newName: "UserId");
            AddColumn("dbo.Topics", "Description", c => c.String());
            AlterColumn("dbo.Topics", "UserId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Topics", "UserId");
            AddForeignKey("dbo.Topics", "UserId", "dbo.Users", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Topics", "UserId", "dbo.Users");
            DropIndex("dbo.Topics", new[] { "UserId" });
            AlterColumn("dbo.Topics", "UserId", c => c.Guid());
            DropColumn("dbo.Topics", "Description");
            RenameColumn(table: "dbo.Topics", name: "UserId", newName: "User_Id");
            CreateIndex("dbo.Topics", "User_Id");
            AddForeignKey("dbo.Topics", "User_Id", "dbo.Users", "Id");

            
        }
    }
}
