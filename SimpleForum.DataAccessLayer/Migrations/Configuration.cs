namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    using Domain;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SimpleForum.DataAccessLayer.ApplicationContext context)
        {
            Guid forumId = Guid.Parse("BC78B179-0489-4253-AD86-3FD17478EB4B");
            // ������� �����
            context.Forums.AddOrUpdate(new Forum { Id = forumId, Title = "�������� �����" });
            // ������� �������
            context.ForumSections.AddOrUpdate(new ForumSection { Id = Guid.Parse("4AF5F575-80B3-4086-946F-7F2C0293C976"), ForumId = forumId, Title = "����� �������", ShowOrder = 1 });
            context.ForumSections.AddOrUpdate(new ForumSection { Id = Guid.Parse("249A2005-1D24-4B45-91D7-B57FE49CB9A7"), ForumId = forumId, Title = ".NET, C# � ���-���-���", ShowOrder = 2 });
            context.ForumSections.AddOrUpdate(new ForumSection { Id = Guid.Parse("0384CAFC-2C56-4C4B-BA97-E7A77A86E527"), ForumId = forumId, Title = "SQL, PL/SQL, Transact-SQL � ��� �� ���� ���� ������", ShowOrder = 3 });
            context.ForumSections.AddOrUpdate(new ForumSection { Id = Guid.Parse("192DCC59-3760-40D3-981E-A514155595CE"), ForumId = forumId, Title = "Oracle ����������", ShowOrder = 4 });
            context.ForumSections.AddOrUpdate(new ForumSection { Id = Guid.Parse("900E63B9-322A-4C5B-BA35-87C7F519B75F"), ForumId = forumId, Title = "Java, ��� ����� � ���� ����� ...", ShowOrder = 5 });
            // ������� ����

            var userRole = new Domain.Role { Id = Guid.Parse("57917425-94F3-4EC5-BEDC-96D27FA66C36"), Name = "User" };

            context.Roles.AddOrUpdate(userRole);
            context.Roles.AddOrUpdate(new Domain.Role { Id = Guid.Parse("B36CC404-6713-4B44-A34D-9DB936424C8F"), Name = "Administrator" });

            var Russia = new Domain.Country { Id = Guid.Parse("E4E99841-89C4-40CD-9D31-91DC2A720978"), IsoCode = "643", FullName = "���������� ���������", ShortName = "������"};

            // ��������� ������ �����
            context.Countries.AddOrUpdate(Russia);
            context.Countries.AddOrUpdate(new Domain.Country { Id = Guid.Parse("85EC4788-84BC-4B4C-B9A2-60DE30C077A8"), IsoCode = "840", ShortName = "���", FullName = "����������� ����� �������" });
            context.Countries.AddOrUpdate(new Domain.Country { Id = Guid.Parse("EF660445-2DBD-4729-A3B0-569F0C8B81A2"), IsoCode = "112", ShortName = "��������", FullName = "���������� ��������" });
            context.Countries.AddOrUpdate(new Domain.Country { Id = Guid.Parse("A496CCC0-5B34-4029-A93C-9ABB0E5068EC"), IsoCode = "398", ShortName = "���������", FullName = "���������� ���������" });

            // ��������� �������� ����
            // �������
            var male = new Domain.Gender { Id = Guid.Parse("74389FE9-56AB-4BEF-869D-CAD4DC930E9D"), Name = "�������" };
            context.Genders.AddOrUpdate(male);
            //  �������
            var female = new Domain.Gender { Id = Guid.Parse("EE5FD763-2D3E-48EE-9B59-12CCAA44B056"), Name = "�������" };
            context.Genders.AddOrUpdate(female);

            // ������� �������������
            // Id	UserName	Blocked	PasswordHash	SecurityStamp	Email	EmailConfirmed
            //FDA56F98 - A809 - 49DF - 86E0 - 4FA71A45C222 user1   0   AOgr4yeirDA2ytXpzT9ifwEiAgxF54Ii5YsAs8hKcfygO7tTH+BHGhhTMsJvQRlrfQ== 4a4bc605 - 31a7 - 4e0b - 9ea0 - 59cb213e064f user1@example.com NULL
            var person1 = new Domain.Person
            {
                FirstName = "����",
                SecondName = "��������",
                LastName = "������",
                Gender = male,
                BirthDate = DateTime.Parse("06.03.2017"),
                CountryOfLiving = Russia
            };

            var user1 = new Domain.User {
                Id = Guid.Parse("FDA56F98-A809-49DF-86E0-4FA71A45C222"),
                UserName = "user1",
                Blocked = false,
                Email = "user1@example.com",
                PasswordHash = "AOgr4yeirDA2ytXpzT9ifwEiAgxF54Ii5YsAs8hKcfygO7tTH+BHGhhTMsJvQRlrfQ==",
                SecurityStamp = "4a4bc605-31a7-4e0b-9ea0-59cb213e064f",
                EmailConfirmed = false,
                Person = person1
            };
            // ����� ����� ��� ���� ������������
            user1.Roles.Add(userRole);
            context.Users.AddOrUpdate(user1);
        }
    }
}
