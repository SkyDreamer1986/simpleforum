namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailConfirmedField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "EmailConfirmed", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "EmailConfirmed");
        }
    }
}
