namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlllowForumSectionShowOrderBeNull : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ForumSections", "ShowOrder", c => c.Int());
            DropColumn("dbo.Topics", "ShowOrder");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Topics", "ShowOrder", c => c.Int(nullable: false));
            DropColumn("dbo.ForumSections", "ShowOrder");
        }
    }
}
