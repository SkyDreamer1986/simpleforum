namespace SimpleForum.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGenderToPerson : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.People", "GenderId", c => c.Guid());
            CreateIndex("dbo.People", "GenderId");
            AddForeignKey("dbo.People", "GenderId", "dbo.Genders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.People", "GenderId", "dbo.Genders");
            DropIndex("dbo.People", new[] { "GenderId" });
            DropColumn("dbo.People", "GenderId");
            DropTable("dbo.Genders");
        }
    }
}
