﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using Domain;
    using DataAccessLayer.Interfaces;
    public class ForumSectionRepository : Repository<ForumSection>, IForumSectionRepository
    {
        public ForumSectionRepository(ApplicationContext context)
            :base(context)
        {
        }
        ApplicationContext ApplicationContext { get { return this.context as ApplicationContext; } }

        public IEnumerable<ForumSection> GetForumSections(Forum forum)
        {
            return this.ApplicationContext.ForumSections.Where(x => x.Forum == forum).OrderBy(x => x.ShowOrder);
        }
    }
}
