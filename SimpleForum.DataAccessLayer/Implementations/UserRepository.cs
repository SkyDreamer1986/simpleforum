﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using System;
    using Domain;
    using Interfaces;

    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationContext context)
            :base(context)
        {
        }

        public IEnumerable<User> geActivetUsers()
        {
            return this.GetAll();                
        }

        public User GetAnonymous()
        {
            throw new NotImplementedException();
        }

        public User GetUserWithPerson(string userName)
        {
            return Context.Users.Where(u => u.UserName.Equals(userName)).Include("Person").FirstOrDefault();
        }

        public User GetUserWithPerson(Guid userId)
        {
            // return ApplicationContext.Forums.Include(f => f.ForumSections.Select(t => t.Topics.Select(u => u.User))).FirstOrDefault();
            return Context.Users.Where(u => u.Id == userId).Include(p => p.Person).FirstOrDefault();
        }

        public ApplicationContext Context
        {
            get { return context as ApplicationContext; }
        }
    }
}
