﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using SimpleForum.DataAccessLayer.Interfaces;
    using Domain;
    using System.Data.Entity;

    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {
        }

        ApplicationContext ApplicationContext { get { return context as ApplicationContext; } }

        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            return ApplicationContext.Users.Where(u => u.Id == userId).Include(u => u.Roles).FirstOrDefault().Roles.ToList();
        }
    }
}
