﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using Interfaces;
    using Domain;

    public class PostRepository : Repository<Post>, IPostRepository
    {
        public PostRepository(ApplicationContext context)
            :base(context)
        {  
        }

        public ApplicationContext ApplicationContext { get { return this.context as ApplicationContext; } }

        public IEnumerable<Post> GetUserPosts(User user)
        {
            return ApplicationContext.Posts.Where(x => x.User.Id == user.Id).ToList();
        }
    }
}
