﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using SimpleForum.DataAccessLayer.Interfaces;
    using SimpleForum.Domain;

    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(DbContext context) : base(context)
        {
        }
    }
}
