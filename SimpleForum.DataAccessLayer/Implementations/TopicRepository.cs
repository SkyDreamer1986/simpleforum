﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using Domain;
    using DataAccessLayer.Interfaces;
    public class TopicRepository : Repository<Topic>, ITopicRepository
    {
        public TopicRepository(ApplicationContext context)
            : base(context)
        {
        }

        public ApplicationContext ApplicationContext { get { return this.context as ApplicationContext; } }

        public IEnumerable<Topic> GetTopicsOfAuthor(User user)
        {
            throw new NotImplementedException();
        }

        public Topic GetTopicWithPosts(Guid topicId)
        {
            var result = ApplicationContext.Topics.Include("Posts").Where(t => t.Id == topicId).FirstOrDefault();

            foreach (var post in result.Posts)
            {
                post.User = ApplicationContext.Users.Where(x => x.Id == post.UserId).FirstOrDefault();
            }

            return result;
        }
    }
}
