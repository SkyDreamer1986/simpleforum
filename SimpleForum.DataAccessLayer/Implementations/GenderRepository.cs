﻿using System;
using System.Data.Entity;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using Domain;
    using Interfaces;
    
    public class GenderRepository : Repository<Gender>, IGenderRepository
    {
        public GenderRepository(DbContext context) : base(context)
        {
        }
        
        public ApplicationContext ApplicationContext { get { return context as ApplicationContext; } }

        public Gender GetFemale()
        {
            return ApplicationContext.Genders.Find(Guid.Parse("EE5FD763-2D3E-48EE-9B59-12CCAA44B056"));
        }

        public Gender GetMale()
        {
            return ApplicationContext.Genders.Find(Guid.Parse("74389FE9-56AB-4BEF-869D-CAD4DC930E9D"));
        }
    }
}
