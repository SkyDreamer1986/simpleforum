﻿using System.Linq;
using System.Data.Entity;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using Domain;
    using Interfaces;

    public class ForumRepository : Repository<Forum>, IForumRepository
    {
        public ForumRepository(ApplicationContext applicationContext)
            : base(applicationContext)
        {
        }

        public ApplicationContext ApplicationContext { get { return this.context as ApplicationContext; } }

        public Forum GetCurrentForum(bool includeSections, bool includeTopics)
        {
            return ApplicationContext.Forums.Include(f => f.ForumSections.Select(t => t.Topics.Select(u => u.User))).FirstOrDefault();
        }
    }
}
