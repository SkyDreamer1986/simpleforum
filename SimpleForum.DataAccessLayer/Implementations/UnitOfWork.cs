﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleForum.DataAccessLayer.Interfaces;

namespace SimpleForum.DataAccessLayer.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {
        ApplicationContext context;
        public UnitOfWork(ApplicationContext context)
        {
            this.context = context;

            this.context.Configuration.LazyLoadingEnabled = false;
            
            Forums = new ForumRepository(context);
            ForumSections = new ForumSectionRepository(context);
            Topics = new TopicRepository(context);
            Posts = new PostRepository(context);
            Users = new UserRepository(context);
            Persons = new PersonRepository(context);
            Genders = new GenderRepository(context);
            Countries = new CountryRepository(context);
            
            Roles = new RoleRepository(context);
            Claims = new ClaimRepository(context);
        }

        public static UnitOfWork Create(ApplicationContext context)
        {
            return new UnitOfWork(context);
        }

        public IRoleRepository Roles { get; set; }

        public IPostRepository Posts { get; private set; }

        public IForumRepository Forums { get; private set; }

        public IForumSectionRepository ForumSections { get; private set; }
        
        public IUserRepository Users { get; private set; }

        public IGenderRepository Genders { get; private set; }

        public IPersonRepository Persons { get; set; }

        public ICountryRepository Countries { get; set; }

        public IClaimRepository Claims { get; set; }

        public ITopicRepository Topics { get; private set; }
        
        public int Complete()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public Task<int> CompleteAsync()
        {
            return context.SaveChangesAsync();
        }
    }
}