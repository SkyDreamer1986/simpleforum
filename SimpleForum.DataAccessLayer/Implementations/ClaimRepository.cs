﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Implementations
{
    using Domain;
    using DataAccessLayer.Interfaces;
    using System.Data.Entity;

    public class ClaimRepository : Repository<Claim>, IClaimRepository
    {
        public ClaimRepository(DbContext context) : base(context)
        {
        }
    }
}
