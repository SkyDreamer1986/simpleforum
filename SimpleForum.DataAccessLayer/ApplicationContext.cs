﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace SimpleForum.DataAccessLayer
{
    using Domain;

    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("SimpleForum")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationContext, Migrations.Configuration>("SimpleForum"));
        }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        } 

        public DbSet<User> Users { get; set; }

        public DbSet<Gender> Genders { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Claim> Claims { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Topic> Topics { get; set; }

        public DbSet<ForumSection> ForumSections { get; set; }

        public DbSet<Forum> Forums { get; set; }

    }
}
