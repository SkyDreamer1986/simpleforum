﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer
{
    using Interfaces;
    public interface IUnitOfWork : IDisposable
    {
        IRoleRepository Roles { get; }

        IUserRepository Users { get; }


        IGenderRepository Genders { get; }

        IPersonRepository Persons { get; }

        ICountryRepository Countries { get; }

        IClaimRepository Claims { get; }

        IForumRepository Forums { get; }

        IForumSectionRepository ForumSections { get; }

        ITopicRepository Topics { get; }

        IPostRepository Posts { get; }

        int Complete();

        Task<int> CompleteAsync();
    }
}
