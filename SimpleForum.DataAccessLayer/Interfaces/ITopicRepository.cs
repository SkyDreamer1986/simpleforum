﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using Domain;
    public interface ITopicRepository : IGenericRepository<Topic>
    {
        IEnumerable<Topic> GetTopicsOfAuthor(User user);

        Topic GetTopicWithPosts(Guid topicId);

    }
}
