﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using Domain;
    public interface IForumRepository : IGenericRepository<Forum>
    {
        Forum GetCurrentForum(bool includeSections, bool includeTopics);
    }
}
