﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using Domain;
    public interface IRoleRepository : IGenericRepository<Role>
    {
        IEnumerable<Role> GetUserRoles(Guid userId);
    }

    



}
