﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using SimpleForum.Domain;
    public interface ICountryRepository : IGenericRepository<Country>
    {
    }
}
