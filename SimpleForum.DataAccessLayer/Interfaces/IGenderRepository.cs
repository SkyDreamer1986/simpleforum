﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using Domain;
    using DataAccessLayer;
    
    public interface IGenderRepository : IGenericRepository<Gender>
    {
        Gender GetMale();
        Gender GetFemale();
    }
}
