﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using Domain;
    public interface IForumSectionRepository : IGenericRepository<ForumSection>
    {
        IEnumerable<ForumSection> GetForumSections(Forum forum);
    }
}
