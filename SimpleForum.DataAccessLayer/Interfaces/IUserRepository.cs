﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleForum.DataAccessLayer.Interfaces
{
    using Domain;
    using Implementations;
    public interface IUserRepository : IGenericRepository<User>
    {
        IEnumerable<User> geActivetUsers();

        User GetAnonymous();

        User GetUserWithPerson(string userName);

        User GetUserWithPerson(Guid userId);
    }
}
